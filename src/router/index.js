import { createRouter, createWebHistory } from 'vue-router';
import HomePage from '@/views/home/home_page.vue';
import HomeList from '@/views/home/home_list/home_list.vue';
import HomeDetail from '@/views/home/home_detail/home_detail.vue';
import HomeEdit from '@/views/home/home_edit/home_edit.vue';
import AuthPage from '@/views/auth/auth_page.vue';
import PageContainer from '@/shared/layout/page_container.vue';
import { useAuthStore } from '@/views/auth/auth_store';

const authStore = useAuthStore();

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    { path: '/', name: 'auth', component: AuthPage },
    { path: '/page', name: 'page', component: PageContainer, children: [
      { path: 'home', name: 'home', component: HomePage, children: [
        { path: 'list', name: 'homeList', component: HomeList },
        { path: 'detail/:id', name: 'homeDetail', component: HomeDetail },
        { path: 'edit/:id', name: 'homeEdit', component: HomeEdit }

      ]
    },
    ] },
  ]
})

router.beforeEach((to, from, next) => {
  if (to.path != '/' && !authStore.hasToken()) {
      next();
    } else {
      next('/');
    }
  }
)


export default router
