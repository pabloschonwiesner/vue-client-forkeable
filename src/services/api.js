import axios from 'axios';

let api = axios.create({
  baseURL: import.meta.env.VITE_URLAPI,
  timeout: 15000,
  headers: {'Content-Type': 'application/json', 'Accept': 'application/json'}
})

export default api
