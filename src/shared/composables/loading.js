import { useLoadingStore } from '@/stores/loading';
import { ElLoading } from 'element-plus';

export function useLoading() {
  const loadingStore = useLoadingStore();

  const openLoading = ( () => {
    loadingStore.setLoading(ElLoading.service({
      lock: true,
      fullscreen: true,
      text: 'Loading',
      background: 'rgba(0, 0, 0, 0.7)',
    }));
  });

  const closeLoading = ( () => {
    loadingStore.closeLoading();
  })
  return { openLoading, closeLoading }
}