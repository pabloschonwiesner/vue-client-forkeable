import { ref } from 'vue'
import { defineStore } from 'pinia'

export const useLoadingStore = defineStore('loading', () => {
  const loading = ref(null);

  function setLoading( value ) {
    loading.value = value;
  }

  function closeLoading() {
    loading.value.close();
  }

  return { closeLoading, setLoading }
})
