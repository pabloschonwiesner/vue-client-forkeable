import { ref } from 'vue'
import { defineStore } from 'pinia'

export const useWidthContentPage = defineStore('widthContentPage', () => {
  const initialValue = 1000;
  const widthPage = ref(initialValue);

  function setWidthPage(value) {
    if(value > initialValue) {
      widthPage.value = value;
    }
  }
  return { widthPage, setWidthPage }
})
