import { reactive } from 'vue';
import { defineStore } from 'pinia';
import { useRouter } from 'vue-router';
import api from '@/services/api';
import { ElNotification } from 'element-plus'
import { useLoading } from '@/shared/composables/loading';

export const useAuthStore = defineStore('authStore', () => {
  const router = useRouter();

  const authData = reactive({
    username: '',
    password: '',
  })

  const authHttp = async () => {
    try {
      useLoading().openLoading();
      const result = await api.post('/login', authData);
      localStorage.setItem('token', result.data.token);
      localStorage.setItem('user', result.data.user);
      ElNotification({
        title: 'Login',
        message: 'Inicio de sesión exitoso.',
        type: 'success'
      })
      router.replace('/page/home/list');
    } catch {
      ElNotification({
        title: 'Inicio de sesión',
        message: 'Error al iniciar sesión!.',
        type: 'error'
      });
    } finally {
      useLoading().closeLoading();
    }
  }

  const closeSession = () => {
    localStorage.removeItem('token');
    localStorage.removeItem('user');
    router.replace('/');
  }

  const hasToken = () => {
    return localStorage.getItem('token') != undefined ? true : false;
  }

  return { 
    authHttp, 
    authData, 
    closeSession,
    hasToken,
  }
})
